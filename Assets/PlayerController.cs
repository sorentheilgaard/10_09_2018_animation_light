﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CharacterController controller;
    private KeyController _keyController;
    public int Speed;
    public int Gravity;
    

    // Use this for initialization
    void Start()
    {
        _keyController = GetComponent<KeyController>();
    }

    private void FixedUpdate()
    {
        Vector3 inputDirection = new Vector3(
        Input.GetAxis("Horizontal"), 0, +Input.GetAxis("Vertical"));
        Vector3 moveDirection = inputDirection * Speed;
        // Apply gravity manually.
        moveDirection.y -= Gravity * Time.deltaTime;
        // Move Character Controller
        controller.Move(moveDirection * Time.deltaTime);
        // Rotate to face move direction
        if (inputDirection.magnitude != 0)
        {
            transform.rotation = Quaternion.LookRotation(inputDirection);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Key")
        {
            _keyController.NumberOfKeysCollected++;
            //_keyController.AddKey(collision.gameObject);

            /*   
            _keyController.NumberOfKeysCollected++;
            */

            Debug.Log(collision.gameObject.name);
            _keyController.NumberOfKeysCollected++;
            Debug.Log(_keyController.NumberOfKeysCollected);


        }
    }

}
