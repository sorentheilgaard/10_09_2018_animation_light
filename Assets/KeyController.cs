﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour {
    public int NumberOfKeysCollected = 0;
    public List<GameObject> KeysCollected = new List<GameObject>();

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddKey(GameObject obj)
    {
        KeysCollected.Add(obj);
    }

}
