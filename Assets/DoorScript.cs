﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public Animator Anim;

    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
            Anim.SetBool("open", true);
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
            Anim.SetBool("open", false);
    }

}
