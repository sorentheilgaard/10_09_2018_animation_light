﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportFromKeyZone : MonoBehaviour {

    public GameObject SpawnPoint;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            collider.transform.position = SpawnPoint.transform.position;
        }
    }
}
