﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmHitAction : MonoBehaviour {

    public Animator Anim;
    public float timeLeft = 0f;

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetButtonDown("Fire1"))
        {
            timeLeft = 0.8f;
            Anim.SetBool("hit", true);	       
        }

	    timeLeft -= Time.deltaTime;

        if (timeLeft < 0)
	    {
	        Anim.SetBool("hit", false);
	    }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "DestroyObject" && timeLeft > 0)
        {
            Destroy(collision.gameObject);
        }
    }




}
