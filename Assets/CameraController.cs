﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public float lookSmooth = 0.1f;
    public Vector3 offsetFromTarget = new Vector3(-1, 5, 0);

    private void Start()
    {
        this.target = GameObject.FindObjectOfType<PlayerController>().transform;
    }
    private void FixedUpdate()
    {
        MoveToTarget();
    }
    private void MoveToTarget()
    {
        transform.position = new Vector3(
            target.position.x + offsetFromTarget.x,
            offsetFromTarget.y,
            target.position.z + offsetFromTarget.z);
    }
}
