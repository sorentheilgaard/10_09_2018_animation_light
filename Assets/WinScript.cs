﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScript : MonoBehaviour
{


    public Canvas CanvasWin;

	// Use this for initialization
	void Start () {
	    CanvasWin.GetComponent<Canvas>().enabled = false;
    }

    void OnTriggerEnter(Collider collider)
    {
        CanvasWin.GetComponent<Canvas>().enabled = true;
    }

    void OnTriggerExit(Collider collider)
    {
        CanvasWin.GetComponent<Canvas>().enabled = false;
    }
}
